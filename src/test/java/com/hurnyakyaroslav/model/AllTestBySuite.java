package com.hurnyakyaroslav.model;
import org.junit.runners.Suite;
import org.junit.runner.RunWith;

@RunWith(Suite.class)
@Suite.SuiteClasses({MinesweeperTest.class, PlateauTest.class})
public class AllTestBySuite {

}
