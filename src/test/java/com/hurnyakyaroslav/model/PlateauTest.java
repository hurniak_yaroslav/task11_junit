package com.hurnyakyaroslav.model;


import org.junit.Assume;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;
import java.util.Collection;


import static org.junit.jupiter.api.Assertions.*;

@RunWith(Parameterized.class)
public class PlateauTest {

    private final static boolean bug = true;

    @Parameterized.Parameter(0)
    public int el1;
    @Parameterized.Parameter(1)
    public int el2;
    @Parameterized.Parameter(2)
    public int el3;
    @Parameterized.Parameter(3)
    public int el4;
    @Parameterized.Parameter(4)
    public int el5;
    @Parameterized.Parameter(5)
    public int el6;
    @Parameterized.Parameter(6)
    public int el7;

    @BeforeAll
    public static void beforeAll() {
        System.out.println("Before all");
    }


    @Test
    public void testArray() {
        LongestPlateau lp = new LongestPlateau(new Integer[]{5, 6, 6, 6, 6, 6, 6, 6, 6, 2, 3, 5});
        int[] result = lp.getMaxSequence();
        int[] teor = {6, 1, 8};
        System.out.println(Arrays.toString(result));
        for (int i = 0; i < 3; i++) {
            assertEquals(result[i], teor[i]);
        }
    }

    @Test
    public void testWithoutSequence() {
        LongestPlateau lp = new LongestPlateau(new Integer[]{6, 6, 6, 6, 6, 2, 3, 5});
        assertThrows(NullPointerException.class, lp::getMaxSequence);
    }


    @Test
    public void testArrayParameterized() {
        LongestPlateau lp = new LongestPlateau(new Integer[]{el1, el2, el3, el4, el5, el6, el7});
        int[] result = lp.getMaxSequence();
        System.out.println(Arrays.toString(result));
    }

    @Test
    public void testWithStaticFinalConstant(){
        if(bug){
            Assume.assumeTrue("False assuming", false);
        }
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getTestData() {
        return Arrays.asList(new Object[][]{
                {2, 6, 6, 6, 6, 5, 7},
                {3, 4, 4, 4, 4, 0, 7},
                {2, 3, 3, 3, 3, 2, 1},
        });
    }

}
