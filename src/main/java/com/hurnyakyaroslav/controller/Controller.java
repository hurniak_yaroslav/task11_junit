package com.hurnyakyaroslav.controller;

import com.hurnyakyaroslav.model.LongestPlateau;
import com.hurnyakyaroslav.model.Minesweeper;

import java.util.Arrays;

public class Controller {
    LongestPlateau lp;
    Minesweeper minesweeper;

    public Controller() {
        lp = new LongestPlateau(new Integer[]{2, 3, 3, 3, 3, 1});
        minesweeper = new Minesweeper(10,10,0.7);
    }

    public String getMaxSequence(){
        return Arrays.toString(lp.getMaxSequence());
    }

    public String getMinesweeperSArea(){
        return (minesweeper.getCharAreaString());
    }
}
