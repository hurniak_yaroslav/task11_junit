package com.hurnyakyaroslav.model;

import java.awt.*;
import java.util.*;

/**
 * Given an array of integers, find the length and location of the longest contiguous
 * sequence of equal values where the values of the elements just before and just after this sequence are
 * smaller.
 */
public class LongestPlateau {
    Integer[] integers;

    private Map<Integer, Point> sequences;

    public LongestPlateau(Integer[] integers) {
        this.integers = integers;
        sequences = new TreeMap<>();
        fillSequences();
    }

    private void fillSequences() {

        for (int i = 1; i < integers.length - 1; i++) {

            int start = 1;
            int end = 1;
            int value = 0;

            if (integers[i + 1].equals(integers[i])) {
                start = i;
                end = i;
                value = integers[i];
            }

            while (integers[i + 1].equals(integers[i]) && i + 1 < integers.length - 1) {
                i++;
                end = i;
            }

            if (integers[i + 1] < integers[i]
                    && integers[start - 1] < integers[i] &&
                    end != start) {
                sequences.put(value, new Point(start, end));
            }
        }
    }


    public int[] getMaxSequence() {

        Map.Entry<Integer, Point> maxEntry =
                ((TreeMap<Integer, Point>) sequences).firstEntry();

        for (Map.Entry<Integer, Point> entry : sequences.entrySet()) {

            Point tmp = entry.getValue();

            if ((tmp.y - tmp.x) > (maxEntry.getValue().y - maxEntry.getValue().x)) {
                maxEntry = entry;
            }
        }
        int[] result;
        if (maxEntry == null) throw new NullPointerException();
        result = new int[]{maxEntry.getKey(), maxEntry.getValue().x, maxEntry.getValue().y};

        return result;
    }


}

