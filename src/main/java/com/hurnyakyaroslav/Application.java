package com.hurnyakyaroslav;

import com.hurnyakyaroslav.view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        new ConsoleView().show();
    }
}
